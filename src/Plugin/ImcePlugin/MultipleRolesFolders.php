<?php

namespace Drupal\imce_multiple_roles_folders\Plugin\ImcePlugin;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\imce\ImcePluginBase;

/**
 * Defines Imce Delete plugin.
 *
 * @ImcePlugin(
 *   id = "multiple_roles_folders",
 *   label = "Multiple Roles Folders",
 *   weight = -5,
 *   operations = {}
 * )
 */
class MultipleRolesFolders extends ImcePluginBase {

  /**
   * Merges folders and permissions for users with multiple roles.
   */
  public function processUserConf(array &$conf, AccountProxyInterface $user) {
    $imce_settings = \Drupal::config('imce.settings');
    $roles_profiles = $imce_settings->get('roles_profiles');
    $user_roles = $user->getRoles();
    $scheme = \Drupal::config('system.file')->get('default_scheme');
    $storage = \Drupal::entityTypeManager()->getStorage('imce_profile');
    $folders_names_permissions = [];
    foreach ($user_roles as $rid) {
      if (!empty($roles_profiles[$rid][$scheme])) {
        if ($profile = $storage->load($roles_profiles[$rid][$scheme])) {
          $imce_role_conf = $profile->getConf();
          if (!empty($imce_role_conf['folders'])) {
            foreach ($imce_role_conf['folders'] as $folder_data) {
              if (!isset($folders_names_permissions[$folder_data['path']])) {
                $folders_names_permissions[$folder_data['path']] = $folder_data['permissions'];
              }
              else {
                $folders_names_permissions[$folder_data['path']] = array_merge($folders_names_permissions[$folder_data['path']], $folder_data['permissions']);
              }
            }
          }
        }
      }
    }
    if (!empty($folders_names_permissions)) {
      if (!isset($conf['folders'])) {
        $conf['folders'] = [];
      }
      foreach ($folders_names_permissions as $folder_name => $folder_permissions) {
        if (!isset($conf['folders'][$folder_name])) {
          $conf['folders'][$folder_name]['permissions'] = $folder_permissions;
        }
        else {
          $conf['folders'][$folder_name]['permissions'] = array_merge($conf['folders'][$folder_name]['permissions'], $folder_permissions);
        }
      }
    }
  }

}
